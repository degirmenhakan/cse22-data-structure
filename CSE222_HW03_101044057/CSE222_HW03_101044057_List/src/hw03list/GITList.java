/***********************************/
/*		Hakan DEGIRMEN	101044057  */
/***********************************/
package hw03list;
/*********************************/
/*			INTERFACE			 */
/*********************************/

/**
 *
 * @author hakan
 */
public interface GITList<E> {
    
    public void add(int index,E obj);
    public void addFirst(E item);
    public void addLast(E item);
    public E get(int index);
    public E getFirst();
    public E getLast();
    public java.util.Iterator<E> iterator();
    public java.util.ListIterator<E>listIterator();
    public java.util.ListIterator<E>listIterator(int index);
    public boolean addAll(GITList<E> l);
    public boolean containsAll(GITList<E> l);
    public boolean removeAll(GITList<E> l);
    public boolean retainAll(GITList<E> l);
    
    
}
