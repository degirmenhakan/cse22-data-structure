/*******************************************/
/*		Hakan DEGIRMEN	101044057  */
/*******************************************/
package hw03list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

/**
 *
 * @author hakan
 */
public class GITArrayList<E> implements GITList<E> {

    private KWArrayList kwlist;

    public GITArrayList() {
        kwlist = new KWArrayList();

    }
    
    /*verilen indexe eleman eklenir kalanlar kaydırılır*/

    @Override
    public void add(int index, E obj) {

        if (index == kwlist.size()) {
            kwlist.add(obj);
        } else if (index < kwlist.size()) {
            E[] tempList;
            tempList = (E[]) new Object[kwlist.size()];

            int size = 0;
            for (int i = 0; i < kwlist.size(); ++i) {
                tempList[i] = (E) kwlist.get(i);
                size++;
            }

            for (int i = 0; i < kwlist.size(); ++i) {

                if (i >= index) {
                    kwlist.set(index, obj);
                    kwlist.set(i, tempList[i - 1]);

                } else {
                    kwlist.set(i, tempList[i]);
                }
            }
            kwlist.add(tempList[size - 1]);

        } else {

            throw new ArrayIndexOutOfBoundsException("Arrayin boyutunu astınız!!");
        }
    }

	/*listenin basına eleman eklenir kalanlar kaydırılır*/
    @Override
    public void addFirst(E item) {
        if (kwlist.size() == 0) {
            kwlist.add(item);
        } else {
            E[] tempList = null;
            tempList = (E[]) new Object[kwlist.size()];

            int size = 0;
            for (int i = 0; i < kwlist.size(); ++i) {
                tempList[i] = (E) kwlist.get(i);
                size++;
            }
            kwlist.set(0, item);
            for (int i = 0; i < kwlist.size() - 1; ++i) {
                kwlist.set(i + 1, tempList[i]);
            }

            kwlist.add(tempList[size - 1]);

        }
    }

	/*listenin sonuna eleman eklenir*/
    @Override
    public void addLast(E item) {
        kwlist.add(item);
    }

	/*ilk eleman donderilir*/
    @Override
    public E getFirst() {
        return (E) kwlist.get(0);
    }

	/*son eleman donderilir*/
    @Override
    public E getLast() {
        return (E) kwlist.get(kwlist.size());
    }

	/*arraylist uzerinden iterator donderilir*/
    @Override
    public Iterator<E> iterator() {
        ArrayList<E> tempList = new ArrayList<E>();
        for (int i = 0; i < kwlist.size(); ++i) {
            tempList.add((E) kwlist.get(i));
        }
        return tempList.iterator();
    }
	/*arraylist uzerinden listiterator donderilir*/
    @Override
    public ListIterator<E> listIterator() {
        ArrayList<E> tempList = new ArrayList<E>();
        for (int i = 0; i < kwlist.size(); ++i) {
            tempList.add((E) kwlist.get(i));
        }

        return tempList.listIterator();
    }
	/*arraylist uzerinden indexinci listiterator donderilir*/
    @Override
    public ListIterator<E> listIterator(int index) {
        ArrayList<E> tempList = new ArrayList<E>();
        for (int i = 0; i < kwlist.size(); ++i) {
            tempList.add((E) kwlist.get(i));
        }
        return tempList.listIterator(index);

    }
	/*verilen liste ana listeye eklenir*/
    @Override
    public boolean addAll(GITList<E> l) {

        for (int i = 0; l.iterator().hasNext(); ++i) {
            try {
                kwlist.add(l.get(i));
                l.iterator().next();
            } catch (Exception e) {
                break;

            }
        }
        return true;
    }

	/*verilen liste ana listede varmı kontrol edilir */
    @Override
    public boolean containsAll(GITList<E> l) {
        int counter = 0;
        int lSize = 0;
        int index;
		/*listenin boyutu hesaplanıyor*/
        for(index=0;;++index){
            try{
                l.get(index);
                ++index;
            }catch(Exception e){
                break;
            }
        
        }
        
        lSize=index;
        
        for (int i = 0; i < kwlist.size(); ++i) {
            for (int j = 0;j<=lSize; ++j) {
                try {
                    
                    if (kwlist.get(i).equals(l.get(j))) {
                        counter++;
                        if(counter == lSize)
                            return true;
                    }
                } catch (Exception e) {
                    break;
                }
            }

        }

        return (counter == lSize);

    }
	/*verilen liset ana listeden cıkartılır*/
    @Override
    public boolean removeAll(GITList<E> l) {
        int counter = 0;
        int lSize = 0;
        int index;
		/*liste boyu hesaplanılıyor*/
        for(index=0;;++index){
            try{
                l.get(index);
                ++index;
            }catch(Exception e){
                break;
            }
        
        }
        
        lSize=index;
  
        for (int i = 0; i < kwlist.size(); ++i) {
            for (int j = 0; j<lSize+1; ++j) {

                try { 
                    if (kwlist.get(i).equals(l.get(j))) {
                        
                        counter++;
                        kwlist.remove(i);
                        
                    }
                } catch (Exception e) {
                    
                    break;
                }   
            }
        }
        
        return (counter == lSize);
    }
	/*verilen liste dısındaki tum elemanlar ana listeden cıkartılır*/
    @Override
    public boolean retainAll(GITList<E> l) {
        int lSize = 0;
        int index;
        for(index=0;;++index){
            try{
                l.get(index);
                ++index;
            }catch(Exception e){
                break;
            }
        
        }
        
        lSize=index;
        
        for (int i = 0; i < kwlist.size(); ++i) {
            
            for (int j = 0; j<lSize; ++j) {
               
                if (!kwlist.get(i).equals(l.get(j))) {
                        kwlist.remove(i);
                    System.out.println(""+lSize+"  "+ kwlist.size());
                    if(kwlist.size()==lSize)
                        return containsAll(l);
                        
                }

            }
            if(kwlist.size()==lSize)
                return containsAll(l);

        }
        
        if(kwlist.size()==lSize)
                return containsAll(l);
        return false;
    }

	/*indexinci eleman donderilir*/
    @Override
    public E get(int index) {
        return (E) kwlist.get(index);
    }
	/*tün elemanları ekrana bastirir*/
    public void print() {
        for (int i = 0; i < kwlist.size(); ++i) {
            System.out.println("" + kwlist.get(i));
        }

    }
}
