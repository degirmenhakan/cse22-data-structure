/*******************************************/
/*		Hakan DEGIRMEN	101044057  */
/*******************************************/
package hw03list;

import java.util.Iterator;
import java.util.ListIterator;

/**
 *
 * @author hakan
 */
public class GITLinkedList<E> implements GITList {

    
    KWLinkedList kwll= new KWLinkedList();
    @Override
	/*verilen elemani istenilen indexe ekliyor*/
    public void add(int index, Object obj) {
        kwll.add(index, obj);
    }

	/*verilen objeyi listenin basına ekliyor*/
    @Override
    public void addFirst(Object item) {
       
        kwll.addFirst(item);
    }
	/*verilen objeyi listenin sonuna ekliyor*/
    @Override
    public void addLast(Object item) {
        kwll.addLast(item);
    }
/*istenilen indexdeki elemanı veriyor*/
    @Override
    public Object get(int index) {
        return (E)kwll.get(index);
    }

	/*ilk elemanı veriyor*/
    @Override
    public Object getFirst() {
       return (E) kwll.getFirst();
    }

	/*listenin son elemanını veriyor*/
    @Override
    public Object getLast() {
        return (E)kwll.getLast();
    }

	/*javanın iteratoru kullanılıyor kwll uzerinden*/
    @Override
    public Iterator<E> iterator() {
       
        
        return kwll.iterator();
    }
/*javanın listiteratoru kullanılıyor kwll uzerinden*/
    @Override
    public ListIterator<E> listIterator() {
        
        return kwll.listIterator();
    }
	/*javanın listiteratoru kullanılarak indexdeki list iterator donderiliyor kwll uzerinden*/
    @Override
    public ListIterator listIterator(int index) {
        
        return kwll.listIterator(index);
    }

	/*verilen listedeki tum elemanlar listeye ekleniyor*/
    @Override
    public boolean addAll(GITList l) {
        int lSize=0;
        /*lsitenin boyu hesaplanılıyor*/
        while(true){
            try{
   
                l.get(lSize);
                lSize++;
            }catch(Exception e){
                break;
            }
        
        }
      	/*ekleme islemi*/
        for(int i=0;i<lSize;++i){
            kwll.addLast(l.get(i));
        
        }
        return true;
    }

	/*verilen listedeki tum elemanlar ana listede varsa true yoksa false donderiliyor*/
    @Override
    public boolean containsAll(GITList l) {
        int lSize=0;
        int kwllSize = 0;
        int counter=0;
		/*listenin uzunlugu*/
        while(true){
            try{
   
                l.get(lSize);
                lSize++;
            }catch(Exception e){
                break;
            }
        }
		/*ana listenin uzunlugu*/	
        while(true){
            try{
   
                kwll.get(kwllSize);
                kwllSize++;
            }catch(Exception e){
                break;
            }
        }
		/*arama hesaplama*/
        for(int i=0;i<kwllSize;++i){
            for(int j=0;j<lSize;++j){
                System.out.println(""+kwllSize+" "+lSize);
                if(kwll.get(i).equals(l.get(j)))
                    counter++;
            
            }
        
        
        }
        return (counter==lSize);
        
    }
	/*verilen listedeki tum elemanlar ana listeden cıkartılıyor*/
    @Override
    public boolean removeAll(GITList l) {
        int counter=0;
        int size=0;
        for(int i=0;kwll.iterator().hasNext();++i,kwll.iterator().next()){
            for(int j=0;l.iterator().hasNext();l.iterator().next(),++j){
                if(kwll.get(i).equals(l.get(i))){
                    kwll.iterator().remove();
                    counter++;
                    
                }
                size++;
            
            
            }
            if(counter==size)
                   return true;
        
        }
        if(counter==size)
                   return true;
        else
                   return false;
        
        
    }
	/*listedeki elemanların dısındaki tum elemanlar listeden cıkartılır*/
    @Override
    public boolean retainAll(GITList l) {
        int size=0;
        int kwllSize=0;
        for(int i=0;kwll.iterator().hasNext();++i,kwll.iterator().next()){
            for(int j=0;l.iterator().hasNext();l.iterator().next(),++j){
                if(kwll.get(i)!=l.get(i)){
                    kwll.iterator().remove();     
                }
                size++;
            
            
            }
           
            kwllSize++;
        }
        if(kwllSize==size)
                   return true;
        else
                   return false;
        
        
    }
    
    
 
}
