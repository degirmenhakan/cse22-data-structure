/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package HW03_1001044057_Test;

import hw03list.GITLinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hakan
 */
public class GITLinkedListTest {
    
    public GITLinkedListTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void AddFirstTest(){
        GITLinkedList<String> gitLinkedList= new GITLinkedList<String>();
        gitLinkedList.addFirst("Hakan");
        assertSame("Hakan", gitLinkedList.get(0));
   
    }
    @Test
    public void AddTest(){
        GITLinkedList<String> gitLinkedList= new GITLinkedList<String>();
        gitLinkedList.addFirst("Hakan");
        gitLinkedList.add(1,"Degirmen");
        gitLinkedList.addFirst("Levent");
        assertSame("Degirmen", gitLinkedList.get(2));
   
    }
    @Test
    public void GetTest(){
        GITLinkedList<String> gitLinkedList= new GITLinkedList<String>();
        gitLinkedList.addFirst("Hakan");
        gitLinkedList.add(1,"Degirmen");
        gitLinkedList.addFirst("Levent");
        assertSame("Levent", gitLinkedList.get(0));
   
    }
    
    @Test
    public void AddAllTest(){
        GITLinkedList<String> gitLinkedList= new GITLinkedList<String>();
        GITLinkedList<String> addLinkedList= new GITLinkedList<String>();
        gitLinkedList.addFirst("Istanbul");
        gitLinkedList.add(1,"Ankara");
        gitLinkedList.addFirst("Yozgat");
        addLinkedList.add(0,"Manisa");
        addLinkedList.add(1,"Sakarya");
        gitLinkedList.addAll(addLinkedList);
        assertSame("Sakarya", gitLinkedList.get(4));
   
    }
     @Test
    public void RemoveAllTest(){
        GITLinkedList<String> gitLinkedList= new GITLinkedList<String>();
        GITLinkedList<String> removeLinkedList= new GITLinkedList<String>();
        gitLinkedList.addFirst("Istanbul");
        gitLinkedList.add(1,"Ankara");
        gitLinkedList.add(1,"Yozgat");
        gitLinkedList.add(2,"Manisa");
        gitLinkedList.add(1,"Sakarya");
        removeLinkedList.add(0,"Manisa");
        removeLinkedList.add(1,"Sakarya");
        
        assertSame(true,gitLinkedList.removeAll(removeLinkedList));
   
    }
     @Test
    public void ContainAllTest(){
        GITLinkedList<String> gitLinkedList= new GITLinkedList<String>();
        GITLinkedList<String> containList= new GITLinkedList<String>();
        gitLinkedList.addFirst("Istanbul");
        gitLinkedList.add(1,"Ankara");
        gitLinkedList.add(1,"Yozgat");
        gitLinkedList.add(2,"Manisa");
        gitLinkedList.add(1,"Sakarya");
        containList.add(0,"Manisa");
        containList.add(1,"Sakarya");
        
        assertSame(true,gitLinkedList.containsAll(containList));
   
    }
      @Test
    public void ContainAllTest2(){
        GITLinkedList<String> gitLinkedList= new GITLinkedList<String>();
        GITLinkedList<String> containList= new GITLinkedList<String>();
        gitLinkedList.addFirst("Istanbul");
        gitLinkedList.add(1,"Ankara");
        gitLinkedList.add(1,"Yozgat");
        gitLinkedList.add(2,"Manisa");
        gitLinkedList.add(1,"Sakarya");
        containList.add(0,"Manisa");
        containList.add(1,"Hatay");
        
        assertSame(false,gitLinkedList.containsAll(containList));
   
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
