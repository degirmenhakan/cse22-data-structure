package HW03_1001044057_Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import hw03list.GITArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import sun.nio.cs.ext.GB18030;

/**
 *
 * @author Hakan
 */
public class GITArrayListTest {
    
    public GITArrayListTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void AddFirstTest(){
        GITArrayList<String> gitArrayList = new GITArrayList<String>();
        gitArrayList.add(0, "Fizik");
        gitArrayList.addFirst("Kimya");
       assertSame("Kimya",gitArrayList.get(0));

    }
    @Test
    public void AddTest(){
        GITArrayList<String> gitArrayList = new GITArrayList<String>();
        gitArrayList.add(0, "Fizik");
        gitArrayList.addFirst("Kimya");
        gitArrayList.add(1, "Geometri");
       assertSame("Fizik",gitArrayList.get(2));

    }
    @Test
    public void AddLastTest(){
        GITArrayList<String> gitArrayList = new GITArrayList<String>();
        gitArrayList.add(0, "Fizik");
        gitArrayList.addFirst("Kimya");
        gitArrayList.addLast( "Geometri");
        assertSame("Geometri",gitArrayList.get(2));

    }
    @Test
    public void IteratorTest(){
        GITArrayList<String> gitArrayList = new GITArrayList<String>();
        gitArrayList.add(0, "Fizik");
        gitArrayList.addFirst("Kimya");
        gitArrayList.addLast( "Geometri");
        assertSame("Kimya",gitArrayList.iterator().next());

    }
    @Test
    public void AddAllTest(){
        GITArrayList<String> gitArrayList = new GITArrayList<String>();
        GITArrayList<String> addList = new GITArrayList<String>();
        addList.add(0, "Türkce");
        addList.add(1,"Almanca");
        System.out.println(""+addList.get(1));
        gitArrayList.add(0, "Fizik");
        gitArrayList.addFirst("Kimya");
        gitArrayList.addLast( "Geometri");
        gitArrayList.addAll(addList);
       assertSame("Türkce",gitArrayList.get(3));

    }
    @Test
    public void RemoveAllTest(){
        GITArrayList<String> gitArrayList = new GITArrayList<String>();
        GITArrayList<String> removeList = new GITArrayList<String>();
        removeList.add(0, "Türkce");
        removeList.add(1,"Almanca");
        
        gitArrayList.addFirst("Kimya");
        gitArrayList.add(1, "Fizik");
        gitArrayList.add(2,"Türkce");
        gitArrayList.add(3, "Almanca");
        gitArrayList.add(4,"Geometri");
 
       assertSame(true,gitArrayList.removeAll(removeList));
       

    }
    @Test
    public void ContainAll(){
        GITArrayList<String> gitArrayList = new GITArrayList<String>();
        GITArrayList<String> containList = new GITArrayList<String>();
        containList.add(0, "Türkce");
        containList.add(1,"Almanca");
        
        gitArrayList.add(0, "Fizik");
        gitArrayList.add(1,"Kimya");
        gitArrayList.add(2,"Geometri");
        gitArrayList.add(3,"Türkce");
        gitArrayList.add(4, "Almanca");
        
       assertSame(true,gitArrayList.containsAll(containList));

    }
    
    
}
