/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw004;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class GITExpression {

    List explist = null;
    String[] strExp = new String[50];
    List varList = null;
    String[] variables;

    List<GITVariable> variableList = new LinkedList<>();

    public GITExpression(String[] str) {
        explist = new LinkedList<String>();
        varList = new LinkedList<String>();

        for (int i = 0; i < str.length; ++i) {

            strExp[i] = str[i];

        }

    }

    public void parser() {
        int k = 0;//ifade kolon
        int t = 0;//ifade satir

        try {
            for (int i = 0; !strExp[i].equals(null); ++i) {

                String str = strExp[i];
                
                if (!str.equals(null)) {
                    for (int j = 0; j < str.length(); ++j) {

                        char ch = str.charAt(j);

                        if (ch == ' ') {
                            String subString = str.substring(t, j);
                            //System.err.println("Str " + subString);
                            explist.add(subString);
                            t = j + 1;

                        }

                    }
                    explist.add(str.substring(t, str.length()));
                    t = 0;
                    explist.add(";");
                }
                
            }
          
        } catch (NullPointerException e) {

        }

    }

    public void fillVarList() {
        Iterator<String> itr = explist.iterator();
        String strVar = null;
        while (itr.hasNext()) {
            if (itr.next().equals("var")) {
                if (itr.hasNext()) {
                    strVar = itr.next();
                    if (!isVar(strVar) && varKontrol(strVar)) {
                        varList.add(strVar);
                    }

                }

            }
        }

    }

    public boolean varKontrol(String strVar) {

        int counter = 0;
        for (int i = 0; i < strVar.length(); ++i) {
            char ch = strVar.charAt(i);
            if ((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122) || ch == 95) {
                counter++;
            }
        }

        return counter == strVar.length();
    }

    public boolean isVar(String strVar) {

        for (int i = 0; i < varList.size(); ++i) {

            if (varList.get(i).equals(strVar)) {
                return true;
            }

        }
        return false;
    }

    public boolean isLvalue(String str) {
        return isVar(str);
    }

    public boolean isOperator(String str) {
        if (str.equals("+") || str.equals("/") || str.equals("*") || str.equals("-") || str.equals("=")) {
            return true;
        }
        return false;
    }

    public void Basla() {
        parser();
        fillVarList();
        Iterator<String> expItr = explist.iterator();

        while (expItr.hasNext()) {
            String yeniSatir = expItr.next();
            
            if (yeniSatir.equals(";")) {

                if (expItr.hasNext()) {
                    String assingVar = expItr.next();
                    if (isVar(assingVar)) {

                        if (expItr.next().equals("=")) {
                            StringBuilder strBuild = null;
                            strBuild = new StringBuilder();
                            strBuild.append(assingVar + " = ");
                            strBuild.append(expItr.next());
                            while (expItr.hasNext()) {
                                String rValue = expItr.next();
                                if (!rValue.equals(";")) {
                                    strBuild.append(" " + rValue);
                                } else {

                                    Isle(strBuild, assingVar);
                                    strBuild.delete(0, strBuild.length());
                                    break;

                                }
                            }

                        } else {
                            GITExceptions exc = new GITExceptions();
                            exc.UnKnowsOperators(assingVar);
                        }

                    } else if (assingVar.equals("print")) {
                        System.out.println("---------------------");
                        String printVal = expItr.next();
                        int indexVal = 0;

                        boolean isVar = isVar(printVal);
                        System.out.println(""+isVar);
                        if ((indexVal = VariableIndex(printVal)) == -1) {
                            if(isVar)
                                 variableList.add(new GITVariable(printVal,0));
                            
                            if (!isOperator(printVal) && !isFunction(printVal)) {

                                try {
                                    float sayi = Float.valueOf(printVal);
                                    System.out.println(sayi);

                                } catch (NumberFormatException e) {

                                    GITExceptions exc = new GITExceptions();
                                    if (!isVar) {
                                        exc.UnDefine(printVal);
                                    } else {
                                        exc.UninitializedVarianle(printVal);
                                    }
                                }

                            }

                        } else  {
                            System.out.println(variableList.get(indexVal).toString());
                        }

                    }
                } else {
                    System.err.println("--------");

                }
            }

        }
    }

    public void Isle(StringBuilder islem, String var) {
        String strIslem = islem.substring(0, islem.length());
        String[] dataIslem = strIslem.split(" ");
        int i = 0;
        int index = 0;
        int oppIndex = 0;
        int functIndex = 0;
        float fonkSonuc = 0;
        float sayi1 = 0, sonuc = 0;

        for (i = 0; i < dataIslem.length; ++i) {

            if (isOperator(dataIslem[i])) {
                oppIndex = i;
                String opp = dataIslem[i + 1];
                if (isOperator(opp)) {
                    System.out.println("Iki operator yanyana gelemez\n");
                    break;
                } else if (isFunction(opp)) {
                    String funcOpp = dataIslem[i + 2];
                    functIndex = i + 1;
                    fonkSonuc = fonkIslem(funcOpp, dataIslem[functIndex]);
                    sonuc = hesapla(sonuc, fonkSonuc, dataIslem[oppIndex]);

                } else {
                    try {
                        sayi1 = Float.valueOf(opp);

                    } catch (NumberFormatException e) {
                        int indexVar = 0;
                        if ((indexVar = VariableIndex(opp)) != -1) {

                            sayi1 = variableList.get(indexVar).getDeger();

                        } else {
                            GITExceptions exc = new GITExceptions();
                            exc.UnDefine(opp);

                        }
                    }
                    sonuc = hesapla(sonuc, sayi1, dataIslem[oppIndex]);

                }
            } else if (isFunction(dataIslem[i])) {
                functIndex = i;
                fonkSonuc = fonkIslem(dataIslem[i + 1], dataIslem[i]);

            } else if (isLvalue(dataIslem[i])) {
                int indexVar = VariableIndex(dataIslem[i]);
                if (indexVar != -1) {
                    sayi1 = variableList.get(indexVar).getDeger();
                }
            }

        }
        GITVariable variable = new GITVariable(dataIslem[0], sonuc);
        int varindex = VariableIndex(dataIslem[0]);

        if (varindex == -1) {
            variableList.add(variable);
        } else {
            variableList.get(varindex).setDeger(sonuc);
        }

    }

    public float hesapla(float opp1, float opp2, String opp) {
        float sonuc = 0;
        switch (opp) {
            case "+":
                sonuc = opp1 + opp2;
                break;
            case "-":
                sonuc = opp1 - opp2;
                break;
            case "*":
                sonuc = opp1 * opp2;
                break;
            case "/":
                sonuc = opp1 / opp2;
                break;
            case "=":
                sonuc = 0 + opp2;
                break;
            default:
                GITExceptions exp = new GITExceptions();
                exp.UnKnowsOperators(opp);
                break;

        }

        return sonuc;
    }

    public boolean isFunction(String str) {
        String[] fonksiyonlar = {"sin", "cos", "sqrt", "log", "abs", "tan", "exp"};

        for (String s : fonksiyonlar) {
            if (s.equals(str)) {
                return true;
            }

        }
        return false;

    }

    public int VariableIndex(String var) {
        int i = 0;
        for (i = 0; i < variableList.size(); ++i) {

            if (variableList.get(i).getIsim().equals(var)) {
                return i;
            }

        }

        return -1;
    }

    private float fonkIslem(String funcOpp, String funcName) {
        float sonuc = 0;
        float sayi = 0;
        int varIndex = 0;
        try {
            sayi = Float.valueOf(funcOpp);
        } catch (NumberFormatException e) {

            varIndex = VariableIndex(funcOpp);
            if (varIndex != -1 && varIndex < variableList.size()) {
                sayi = variableList.get(varIndex).getDeger();
            } else {
                GITExceptions exp = new GITExceptions();
                exp.UnLvalue(funcOpp);
            }

        }

        switch (funcName) {
            case "sin":
                sonuc = (float) Math.sin(sayi);
                break;
            case "cos":
                sonuc = (float) Math.cos(sayi);
                break;
            case "sqrt":
                sonuc = (float) Math.sqrt(sayi);
                break;
            case "log":
                sonuc = (float) Math.log(sayi);
                break;
            case "abs":
                sonuc = (float) Math.abs(sayi);
                break;
            case "tan":
                sonuc = (float) Math.tan(sayi);
                break;
            case "exp":
                sonuc = (float) Math.exp(sayi);
                break;
            default:
                System.err.println("Tanimsiz Fonksiyon");

        }

        return sonuc;

    }

}
