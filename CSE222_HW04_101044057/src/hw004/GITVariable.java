/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hw004;

/**
 *
 * @author Hakan
 */
public class GITVariable {
    
    private String isim;
    private float deger;
    
    public GITVariable(){
    
    }
    public GITVariable(String isim, float deger) {
        this.isim = isim;
        this.deger = deger;
    }

    public String getIsim() {
        return isim;
    }

    public float getDeger() {
        return deger;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }

    public void setDeger(float deger) {
        this.deger = deger;
    }
    
    public boolean isSame(GITVariable other){
        if(isim.equals(other.getIsim()) && deger == other.getDeger())
            return true;
        return false;
    }
    
    public String toString(){
        return ""+deger;
    }
       
    
}
