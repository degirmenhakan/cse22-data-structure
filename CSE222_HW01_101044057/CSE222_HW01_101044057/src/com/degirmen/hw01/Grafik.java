/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.degirmen.hw01;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

import javax.swing.JPanel;

public class Grafik extends JPanel {

    
     /*********************************************************/
    /*                      DATA MEMBERS                      */
    /**********************************************************/
    private int secenek = 0;
    private Graphics2D grafik;
    private ArrayList<Entity> arr_entity = null;

    /***********************************************************/
    /*                 CONSTRUCTOR                             */
    /***********************************************************/
    public Grafik(ArrayList<Entity> _arr_entity) {
        super();
        //verilen listedeki elemanlari kopyaliyorum
        arr_entity = new ArrayList<>();
        for (int i = 0; i < _arr_entity.size(); i++) {
            Entity entity = new Entity();
            entity.setAlt_Sinir(_arr_entity.get(i).getAltSinir());
            entity.setOperasyon_Adi(_arr_entity.get(i).getOperasyonAdi());
            entity.setUst_Sinir(_arr_entity.get(i).getUstSinir());
            entity.setRenk(_arr_entity.get(i).getRenk());

            arr_entity.add(entity);
        }

    }

    //isleme gore secenek degerimi belirleyip ona gore grafik cizicem
    public void setIslemSecenek(String opp) {

        switch (opp) {
            case "x":
                secenek = 1;
                break;
            case "sin(x)":
                secenek = 2;
                break;
            case "cos(x)":
                secenek = 3;
                break;
            case "log(x)":
                secenek = 4;
                break;
            case "2*x":
                secenek = 5;
                break;
            case "x/2":
                secenek = 6;
                break;
            case "tan(x)":
                secenek = 7;
                break;
            default:
                secenek = 0;
                
                break;
        }

    }

    //verilen renge gore grafigin rengini ayarliyorum
    public void setRenk(String strRenk, int index) {
        if (strRenk.equals("RED")) {
            grafik.setColor(Color.red);
        } else if (strRenk.equals("GREEN")) {
            grafik.setColor(Color.GREEN);
        } else if (strRenk.equals("BLUE")) {
            grafik.setColor(Color.BLUE);
        } else if (strRenk.equals("CYAN")) {
            grafik.setColor(Color.CYAN);
        } else if (strRenk.equals("YELLOW")) {
            grafik.setColor(Color.YELLOW);
        } else {
            arr_entity.get(index).setRenk("BLACK");
            grafik.setColor(Color.BLACK);
        }

    }

    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);

        grafik = (Graphics2D) g;
        //x ve y eksenim
        grafik.drawLine(0, 180, 360, 180);
        grafik.drawLine(180, 0, 180, 360);

        //tum fonksiyonları cizecegimiz icin tum listedekileri aliyorum
        for (int j = 0; j < arr_entity.size(); j++) {
            setIslemSecenek(arr_entity.get(j).getOperasyonAdi().trim());
            if (secenek == 1) { //x ise
                setRenk(arr_entity.get(j).getRenk(), j);
                grafik.drawLine(180 + (int) (30 * arr_entity.get(j).getAltSinir()), 180 - (int) (30 * arr_entity.get(j).getAltSinir()), 180 + (int) (30 * arr_entity.get(j).getUstSinir()), (int) (180 - 30 * arr_entity.get(j).getUstSinir()));

            } else if (secenek == 2) {//sin(x) ise
                for (int i = 0; i < (arr_entity.get(j).getUstSinir() - arr_entity.get(j).getAltSinir()) * 30 * 6; i++) {
                    int x = (int) (180 + arr_entity.get(j).getAltSinir() * 30 + i) / 6;
                    int y = (int) (180 - Math.sin(Math.toRadians(i)) * 30);
                    setRenk(arr_entity.get(j).getRenk(), j);
                    grafik.drawLine(x, y, x, y);
                }

            } else if (secenek == 3) {//cos(x) ise
                for (int i = 0; i < (arr_entity.get(j).getUstSinir() - arr_entity.get(j).getAltSinir()) * 30 * 6; ++i) {
                    int x = (int) (180 + arr_entity.get(j).getAltSinir() * 30 + i) / 6;
                    int y = (int) (180 + Math.cos(Math.toRadians(i)) * 30);
                    setRenk(arr_entity.get(j).getRenk(), j);
                    grafik.drawLine(x, y, x, y);
                }

            } else if (secenek == 4) {//log(x) ise
                if (arr_entity.get(j).getAltSinir() < 0) {//x sifirdan kucuk olamaz
                    System.out.println("Logarithmic function can not be negative range.");
                   
                } else {
                    for (int i = 0; i < (arr_entity.get(j).getUstSinir() - arr_entity.get(j).getAltSinir()) * 30; ++i) {
                        int x = (int) (arr_entity.get(j).getAltSinir() * 30 + i);
                        int y = (int) (Math.log((x)) * 6);
                        setRenk(arr_entity.get(j).getRenk(), j);
                        grafik.drawLine(180 + x, 180 - y, 180 + x, 180 - y);
                    }
                }
            } else if (secenek == 5) {//2*x
                setRenk(arr_entity.get(j).getRenk(), j);
                grafik.drawLine((int) (180 + arr_entity.get(j).getAltSinir() * 30), (int) (180 - arr_entity.get(j).getAltSinir() * 30 * 2), (int) (180 + arr_entity.get(j).getUstSinir() * 30), ((int) (180 - arr_entity.get(j).getUstSinir() * 30 * 2)));

            } else if (secenek == 6) {//x/2
                setRenk(arr_entity.get(j).getRenk(), j);
                grafik.drawLine((int) (180 + arr_entity.get(j).getAltSinir() * 30), (int) (180 - arr_entity.get(j).getAltSinir() * 30 / 2), (int) (180 + arr_entity.get(j).getUstSinir() * 30), (int) (180 - arr_entity.get(j).getUstSinir() * 30 / 2));
            } else if (secenek == 7) {//tan(x)
                for (int i = 0; i < (arr_entity.get(j).getUstSinir() - arr_entity.get(j).getAltSinir()) * 30 * 6; ++i) {
                    int x = (int) (180 + arr_entity.get(j).getAltSinir() * 30 + i) / 6;
                    int y = (int) (180 + Math.tan(Math.toRadians(i)) * 30);
                    setRenk(arr_entity.get(j).getRenk(), j);
                    grafik.drawLine(x, y, x, y);
                }
            }
        }
    }
    //setter-getter
    public int getSecenek() {
        return secenek;
    }

    public void setSecenek(int secenek) {
        this.secenek = secenek;
    }

    public ArrayList<Entity> getEntities() {
        return arr_entity;
    }
}
