/**
 * @author Hakan DEGIRMEN Num:101044057
 * @version 1.0
 * @created 21-Fub-2014 10:31:00
 */
package com.degirmen.hw01;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;


public class Gui implements UI {
    /**********************************************************/
    /*                      DATA MEMBERS                      */
    /**********************************************************/
    private ArrayList<Entity> m_Entity = null;
    private JFrame frameGraph = new JFrame("Graphic");
    private JFrame frame = null;
    private Grafik g1=null;
    
    /**********************************************************/
    /*                CONSTRUCTOR                             */
    /**********************************************************/ 

    public Gui() {
        frameGraph = new JFrame();
        m_Entity = new ArrayList<>();
        dosyadanOku("input.txt");
        menu();
    }

    @Override//dosyadan bilgileri satir satir aliyorum
    public boolean dosyadanOku(String dosya_adi) {

        try {
            try (FileReader dosya = new FileReader(dosya_adi)) {
                BufferedReader oku = new BufferedReader(dosya);

                String data = null;
                int i = 0;
                while ((data = oku.readLine()) != null) {
                    if (!data.equals(null)) {
                        Entity newEntity = new Entity();

                        newEntity = StringParser(data);
                        if (newEntity != null) {
                            m_Entity.add(newEntity);
                            i++;
                        }
                    }
                }
                return true;
            }

        } catch (FileNotFoundException e) {

            System.err.println(" " + e);
            return false;
        } catch (IOException ex) {
            Logger.getLogger(Consol.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    @Override//olsturdugum grafik objesini framee ekliyorum
    public void grafikCiz(ArrayList<Entity> bilgiler) {
        
        frameGraph.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frameGraph.setSize(360, 360);

        g1 = new Grafik(m_Entity);
        frameGraph.add(g1);
        frameGraph.repaint();
        frameGraph.setVisible(true);
    }

    @Override//eger ekrana bastirdildi ise grafik componentini frameden kaldrıyorum
    public void grafikSil() {
        if (frameGraph.isVisible()) {
            frameGraph.remove(g1);
        }
        frameGraph.repaint();

    }

    @Override//dosyadan okudugum bilgileri pars edip entity objesine koyuyorum
    public Entity StringParser(String islem) {

        String name = null;
        String renk = null;
        double alt = 0.0, ust = 0.0;

        String ayrac = ",";
        String[] tumParcalar = null;

        tumParcalar = islem.split(ayrac);
        if (!tumParcalar[0].contains("x")) {
            return null;
        }
        try {
            name = tumParcalar[0];
            alt = Double.valueOf(tumParcalar[1]);
            ust = Double.valueOf(tumParcalar[2]);
            renk = tumParcalar[3];
        } catch (NumberFormatException e) {//dosyaya hatali veri yazildi ise
            System.err.println("File format error...");
        }

        Entity obj = new Entity(name, alt, ust, renk);

        return obj;
    }

    @Override//kullaniciya menu gorevi gorecek frame 
    public int menu() {
        frame = new JFrame("GUI");
        frame.setSize(360, 360);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        final JButton btnCiz = new JButton("Draw");
        btnCiz.setLocation(120, 60);
        btnCiz.setText("Draw");
        btnCiz.setSize(80, 40);
        frame.add(btnCiz);
        JButton btnSil = new JButton("Clean");
        btnSil.setLocation(120, 120);
        btnSil.setText("Clean");
        btnSil.setSize(80, 40);
        frame.add(btnSil);
        JButton btnExit = new JButton("Exit");
        btnExit.setLocation(120, 180);
        btnExit.setText("Exit");
        btnExit.setSize(80, 40);
        frame.add(btnExit);

        frame.setVisible(true);
        //butonlara tıklandıgında yapılacak islemler
        btnCiz.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                grafikCiz(m_Entity);//surekli tekrarla yeni pencere acilmasini engellemek
                //icin grafik ekrani aciksa buton islemez oluyor
                if(frameGraph.isVisible())
                    btnCiz.setEnabled(false);

            }
        });
        btnSil.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                btnCiz.setEnabled(true);
                grafikSil();

            }
        });
        btnExit.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                frame.setVisible(false);
                
                if (frameGraph.isVisible()) {
                    frameGraph.setVisible(false);
                    
                }

            }
        });
        return 0;
    }

}
