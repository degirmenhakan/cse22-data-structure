/**
 * @author Hakan DEGIRMEN Num:101044057
 * @version 1.0
 * @created 21-Fub-2014 10:31:00
 */

package com.degirmen.hw01;


public class Entity {

    /**********************************************************/
    /*                      DATA MEMBERS                      */
    /**********************************************************/
    private double alt_sinir=0;
    private String operasyon_ismi;
    private String renk;
    private double ust_sinir=0;

    /**********************************************************/
    /*                CONSTRUCTORS                            */
    /**********************************************************/
    
    public Entity() {

    }

    public Entity(String _name, double _alt_sinir, double _ust_sinir, String _renk) {

        setOperasyon_Adi(_name);
        setUst_Sinir(_ust_sinir);
        setAlt_Sinir(_alt_sinir);
        setRenk(_renk);

    }
    /****************************************************************/
    /*               SETTER VE GETTER FONKSIYONLARI                 */
    /****************************************************************/

    public double getAltSinir() {
        return alt_sinir;
    }

    public String getOperasyonAdi() {
        return operasyon_ismi;
    }

    public String getRenk() {
        return renk;
    }

    public double getUstSinir() {
        return ust_sinir;
    }

    /**
     *
     * @param _alt_sinir
     */
    public void setAlt_Sinir(double _alt_sinir) {
        /*eger alt sinir ust sinirdan buyuk esitse*/
        if(_alt_sinir>=ust_sinir){
            
            
            alt_sinir=ust_sinir;
        }
        else
            alt_sinir = _alt_sinir;
    }

    /**
     *
     * @param op_adi
     */
    public void setOperasyon_Adi(String op_adi) {
        operasyon_ismi = op_adi;
    }

    /**
     *
     * @param _renk
     */
    public void setRenk(String _renk) {

        renk = _renk;
    }

    /**
     *
     * @param _ust_sinir
     */
    public void setUst_Sinir(double _ust_sinir) {

        ust_sinir = _ust_sinir;

    }

}
