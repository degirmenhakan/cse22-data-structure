package com.degirmen.hw01;

import java.util.ArrayList;



/**
 * @author Hakan DEGIRMEN Num:101044057
 * @version 1.0
 * @created 21-Fub-2014 10:31:00
 */

//Gui ve Consol claslarimin kullanacagi interface
public interface UI {

	
	public boolean dosyadanOku(String dosya_adi);
        
	public void grafikCiz(ArrayList<Entity> bilgiler);

	public void grafikSil();
        
        public Entity StringParser(String islem);

        public int menu();

}