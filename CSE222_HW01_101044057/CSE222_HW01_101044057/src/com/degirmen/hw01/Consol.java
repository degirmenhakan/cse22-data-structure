/**
 * @author Hakan DEGIRMEN Num:101044057
 * @version 1.0
 * @created 21-Fub-2014 10:31:00
 */

package com.degirmen.hw01;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import javax.swing.JFrame;


//consol classım ui interfacei imlement edecek
public class Consol implements UI {

    
    /**********************************************************/
    /*                      DATA MEMBERS                      */
    /**********************************************************/
    private JFrame frame = new JFrame();
    private ArrayList<Entity> m_Entity = null;
    private String dosyaadi = "input.txt";
    private Grafik g1= null;
    
    /**********************************************************/
    /*                CONSTRUCTOR                             */
    /**********************************************************/        
    public Consol() {

        m_Entity = new ArrayList<Entity>();
        dosyadanOku(dosyaadi);
        g1= new Grafik(m_Entity);

    }

    /*Kullanici consoldan islem sececek*/
    public int menu() {
        
        Scanner sayi_al = new Scanner(System.in);
        //kullanici verilen menuden secim yapiyor
        System.out.println("1)Draw to frame\n2)Clean the frame\n3)Exit");
        //kullanici yanlıs aralıkda deger girerse  veya yanlıs karakter girerse
        //exception fırlatacak ve uyarı mesajı verecegım
        try{
        int secenek = sayi_al.nextInt();
        //kullanicinin secimine gore islemleri yapacagim
        if (secenek == 1) {
            grafikCiz(m_Entity);
            return 1;
        } else if (secenek == 2) {
            grafikSil();
            return 2;
        } else if (secenek == 3) {
            if(frame.isVisible())
                frame.setVisible(false);
            return 3;

        } else {//yukaridaki aralıgın dısında ise
            System.out.println("Wrong selection..");
            return -1;
        }
        }catch(InputMismatchException e){//yanlis karakter girdi ise
            System.out.println("Wrong selection..");
        }
        return 0;
    }

    @Override//dosyadan okuma fonksiyonum
    public boolean dosyadanOku(String dosya_adi) {

        m_Entity = new ArrayList<>();//okudugum bilgileri yeni bi entity objesine
                                    //bu yeni objeleri de Arraylistime atiyorum

        try {
            try (FileReader dosya = new FileReader(dosya_adi)) {
                BufferedReader oku = new BufferedReader(dosya);

                String data = null;
                int i = 0;
                while ((data = oku.readLine()) != null) {
                    if (!data.equals(null)) {
                        Entity newEntity = new Entity();

                        newEntity = StringParser(data);
                        if(newEntity!=null){   
                        m_Entity.add(newEntity);
                        i++;}
                    }
                }
                return true;
            }

        } catch (FileNotFoundException e) {//dosya acilamadi ise

            System.err.println(" " + e);
            return false;
        } catch (IOException ex) {
            Logger.getLogger(Consol.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    @Override//grafik bilgisi alınan datalara gore bir g1 objesine atanmis olacak
            //ve g1 objesi framee eklenecek
    public void grafikCiz(ArrayList<Entity> bilgiler) {
        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(360, 360);

        
        frame.add(g1);
        frame.repaint();
        frame.setVisible(true);

    }

    @Override//g1 i frameden kaldırıp frami yenileyecek
    public void grafikSil() {
      frame.remove(g1);
      frame.repaint();
              
       
    }

    @Override//dosyadan okudugum tek satırı virgule gore ayırıp bilgileri aliyorum
    public Entity StringParser(String islem) {

        String name = null;
        String renk = null;
        double alt = 0.0, ust = 0.0;

        String ayrac = ",";
        String[] tumParcalar = null;

        tumParcalar = islem.split(ayrac);
        if (!tumParcalar[0].contains("x")) {
            return null;
        }
        try {
            name = tumParcalar[0];
            alt = Double.valueOf(tumParcalar[1]);
            ust = Double.valueOf(tumParcalar[2]);
            renk = tumParcalar[3];
        } catch (NumberFormatException e) {
            System.err.println("File format error...");
        }

        Entity obj = new Entity(name, alt, ust, renk);

        return obj;
    }
    //tum entityleri bir arada bulunduran Arraylistimi donderiyor
    public ArrayList<Entity> getEntities(){
        return m_Entity;
    }
}
