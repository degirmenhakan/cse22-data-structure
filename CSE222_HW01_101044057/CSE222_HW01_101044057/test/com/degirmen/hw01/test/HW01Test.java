
/**
 *
 * @author Hakan DEGIRMEN Num:101044057
 */

package com.degirmen.hw01.test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.degirmen.hw01.Consol;
import com.degirmen.hw01.Entity;
import com.degirmen.hw01.Grafik;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class HW01Test {
    
    public HW01Test() {
        
    }
    
    

    @Test
    public void ConsolTest1(){
       //dosyaya cos(x),-6,6,GREEN yazdım 
        
        Consol c= new Consol();
        
        double olmasi_gereken=-6.0;
        double kullanilan=c.getEntities().get(0).getAltSinir();
        assertEquals(kullanilan,olmasi_gereken,0.0);
    }

  
    @Test//alt snir ust sinirdan buyukse
    public void EntitySetAltSinirTest1(){
        
        Entity entity=new Entity("x",5,-1,"GREEN");
        double kullanilan=entity.getAltSinir();
        double olmasi_gereken=-1;
        assertEquals(kullanilan, olmasi_gereken,0.0);
    }
    @Test//alt sinir dogru verilmis
    public void EntityGetAltSinirTest1(){
        
        Entity entity=new Entity("x",-5,-1,"GREEN");
        double kullanilan=entity.getAltSinir();
        double olmasi_gereken=-5;
        assertEquals(kullanilan, olmasi_gereken,0.0);
    }
    @Test
    public void EntityTest(){
        Entity entity= new Entity();
        String kullanilan=entity.getOperasyonAdi();
        String olmasi_gereken=null;
        assertEquals(kullanilan, olmasi_gereken);
    
    }
    @Test
    public void setRenkTest(){
        Entity entity=new Entity();
        entity.setRenk("white");
        String kullanilan=entity.getRenk();
        String olmasi_gereken="white";
        assertEquals(kullanilan, olmasi_gereken);
    }
    @Test
    public void GrafikTest(){
        
        Entity entity=new Entity("x",5,-1,"GREEN");
        ArrayList<Entity> entities=new ArrayList<>();
        entities.add(entity);
        Grafik grafik1=new Grafik(entities);
        grafik1.setIslemSecenek("x");
        int kullanilan=grafik1.getSecenek();
        int olmasi_gereken=1; 
        assertEquals(kullanilan, olmasi_gereken,0.0);
    }
    @Test
    public void GrafikTest2(){
        //file format hatasi vermeli
        Entity entity=new Entity("5x",5,-1,"GREEN");
        ArrayList<Entity> entities=new ArrayList<>();
        entities.add(entity);
        Grafik grafik1=new Grafik(entities);
        grafik1.setIslemSecenek("5x");
        int kullanilan=grafik1.getSecenek();
        int olmasi_gereken=0; 
        
        assertEquals(kullanilan, olmasi_gereken,0.0);
    }
    @Test
    public void GrafikTest3(){
        
        Entity entity=new Entity("x",5,-1,"GREEN");
        ArrayList<Entity> entities=new ArrayList<>();
        entities.add(entity);
        Grafik grafik1=new Grafik(entities);
        grafik1.setIslemSecenek("2*x");
        int kullanilan=grafik1.getSecenek();
        int olmasi_gereken=5; 
        
        assertEquals(kullanilan, olmasi_gereken,0.0);
    }
  
}
