package hw05;

import hw05.InfixToPostfix.SyntaxErrorException;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedList;


public class Main {
	
	public static final String fileName="C:\\Users\\Hakan\\workspace\\HW05\\program.git";
	
	@SuppressWarnings("deprecation")
	public static void main(String [] argc) throws IOException, SyntaxErrorException, hw05.PostfixEvaluator.SyntaxErrorException{
		LinkedList<String> list= new LinkedList<String>();
		
		File dosya= new File(fileName);
		FileInputStream read=new FileInputStream(dosya);
		@SuppressWarnings("resource")
		DataInputStream satirOku= new DataInputStream(read);
		String satir;
		do{
			satir=satirOku.readLine();
			list.add(satir);
			
		}while(satir!=null);
		
		Evaluation eval = new Evaluation(list);
		eval.basla();
		
		
	}
	
	
	

}
