package hw05;

import java.util.*;

/**
 * Translates an infix expression to a postfix expression.
 * 
 * @author Koffman & Wolfgang
 * */

public class InfixToPostfix {

	// Nested Class
	/** Class to report a syntax error. */
	public static class SyntaxErrorException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * Construct a SyntaxErrorException with the specified message.
		 * 
		 * @param message
		 *            The message
		 */
		SyntaxErrorException(String message) {
			super(message);
		}
	}

	// Data Fields
	/** The operator stack */
	private Stack<Character> operatorStack;

	/** The operators */
	private static final String OPERATORS = "+-*/()";

	/** The precedence of the operators, matches order in OPERATORS. */
	private static final int[] PRECEDENCE = { 1, 1, 2, 2 ,0,0};

	/** The postfix string */
	private StringBuilder postfix;

	/**
	 * Convert a string from infix to postfix.
	 * 
	 * @param infix
	 *            The infix expression
	 * @throws SyntaxErrorException
	 */
	public String convert(String infix) throws SyntaxErrorException {
		operatorStack = new Stack<Character>();
		postfix = new StringBuilder();
		StringTokenizer infixTokens = new StringTokenizer(infix);
		try {
			// Process each token in the infix string.
			while (infixTokens.hasMoreTokens()) {
				String nextToken = infixTokens.nextToken();
				
				char firstChar = nextToken.charAt(0);
				// Is it an operand?
				if (Character.isJavaIdentifierStart(firstChar)
						|| Character.isDigit(firstChar)
						|| nextToken.equals("sin") == true
						|| nextToken.equals("cos") == true
						|| nextToken.equals("log") == true
						|| nextToken.equals("sqrt") == true
						|| nextToken.equals("abs") == true
						|| nextToken.equals("tan") == true
						|| nextToken.equals("exp") == true) {
					if (nextToken.equals("sin")) {
						try {
							double deger = Double.parseDouble(infixTokens
									.nextToken());
							nextToken = Double.toString(Math.sin(deger));
							postfix.append(nextToken);
							postfix.append(' ');

						} catch (NumberFormatException e) {
							System.out.println("Number Format Exceprion");
							System.out
									.println("Trigonometrik fonksiyon icerisine reel sayi alir");

						}
					} else if (nextToken.equals("cos")) {
						try {
							double deger = Double.parseDouble(infixTokens
									.nextToken());
							nextToken = Double.toString(Math.cos(deger));
							postfix.append(nextToken);
							postfix.append(' ');

						} catch (NumberFormatException e) {
							System.out.println("Number Format Exceprion");
							System.out
									.println("Trigonometrik fonksiyon icerisine reel sayi alir");

						}
					} else if (nextToken.equals("tan")) {
						try {
							double deger = Double.parseDouble(infixTokens
									.nextToken());
							nextToken = Double.toString(Math.tan(deger));
							postfix.append(nextToken);
							postfix.append(' ');

						} catch (NumberFormatException e) {
							System.out.println("Number Format Exceprion");
							System.out
									.println("Trigonometrik fonksiyon icerisine reel sayi alir");

						}
					} else if (nextToken.equals("abs")) {
						try {
							double deger = Double.parseDouble(infixTokens
									.nextToken());
							nextToken = Double.toString(Math.abs(deger));
							postfix.append(nextToken);
							postfix.append(' ');

						} catch (NumberFormatException e) {
							System.out.println("Number Format Exceprion");
							System.out.println("Mutlak degerini alacaginiz deger reel sayi olmalidir.");

						}
					}else if (nextToken.equals("exp")) {
						try {
							double deger = Double.parseDouble(infixTokens
									.nextToken());
							nextToken = Double.toString(Math.exp(deger));
							postfix.append(nextToken);
							postfix.append(' ');

						} catch (NumberFormatException e) {
							System.out.println("Number Format Exceprion");
							System.out.println("Trigonometrik fonksiyon icerisine reel sayi alir");

						}
					}else if (nextToken.equals("log")) {
						try {
							double deger = Double.parseDouble(infixTokens
									.nextToken());
							nextToken = Double.toString(Math.log(deger));
							postfix.append(nextToken);
							postfix.append(' ');

						} catch (NumberFormatException e) {
							System.out.println("Number Format Exceprion");
							System.out.println("Trigonometrik fonksiyon icerisine reel sayi alir");

						}
					}else if (nextToken.equals("sqrt")) {
						try {
							double deger = Double.parseDouble(infixTokens
									.nextToken());
							nextToken = Double.toString(Math.sqrt(deger));
							postfix.append(nextToken);
							postfix.append(' ');

						} catch (NumberFormatException e) {
							System.out.println("Number Format Exceprion");
							System.out.println("Trigonometrik fonksiyon icerisine reel sayi alir");

						}
					}else {
						postfix.append(nextToken);
						postfix.append(' ');
					}
				}// Is it an operator?
				else if (isOperator(firstChar)) {
					processOperator(firstChar);
				} else {
					throw new SyntaxErrorException(
							"Unexpected Character Encountered: " + firstChar);
				}
			} // End while.

			// Pop any remaining operators and
			// append them to postfix.
			while (!operatorStack.empty()) {
				char op = operatorStack.pop();
				if(op=='(')/*stackte acma parantezi kalmissa*/{
					throw new SyntaxErrorException("Unmatched parenthesis");
					
					
				}
				postfix.append(op);
				postfix.append(' ');
			}
			// assert: Stack is empty, return result.
			return postfix.toString();
		} catch (EmptyStackException ex) {
			throw new SyntaxErrorException("Syntax Error: The stack is empty");
		}
	}

	/**
	 * Method to process operators.
	 * 
	 * @param op
	 *            The operator
	 * @throws EmptyStackException
	 */
	private void processOperator(char op) {
		
		if (operatorStack.empty() || op=='(') {
			operatorStack.push(op);
		} else {
			// Peek the operator stack and
			// let topOp be top operator.
			char topOp = operatorStack.peek();
			if (precedence(op) > precedence(topOp)) {
				operatorStack.push(op);
			} else {
				// Pop all stacked operators with equal
				// or higher precedence than op.
				while (!operatorStack.empty()
						&& precedence(op) <= precedence(topOp)) {
					operatorStack.pop();
					if(topOp=='(')
						break;
					postfix.append(topOp);
					postfix.append(' ');
					if (!operatorStack.empty()) {
						// Reset topOp.
						topOp = operatorStack.peek();
					}
				}
				
				// assert: Operator stack is empty or
				// current operator precedence >
				// top of stack operator precedence.
				if(op !=')')/*eger operator kapama parantezi degilse o operatoru geri koy*/
					operatorStack.push(op);
			}
		}
	}

	/**
	 * Determine whether a character is an operator.
	 * 
	 * @param ch
	 *            The character to be tested
	 * @return true if ch is an operator
	 */
	private boolean isOperator(char ch) {
		return OPERATORS.indexOf(ch) != -1;
	}

	/**
	 * Determine the precedence of an operator.
	 * 
	 * @param op
	 *            The operator
	 * @return the precedence
	 */
	private int precedence(char op) {
		return PRECEDENCE[OPERATORS.indexOf(op)];
	}
}
