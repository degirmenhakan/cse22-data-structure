package hw05;

public class Variable {
	
	private double deger=0;
	private String name;
	private boolean init;
	
	
	public Variable(double deger, String name) {
		super();
		this.deger = deger;
		this.name = name;
		this.init=true;
	}
	public Variable(String _name){
		this.name=_name;
		this.deger=0;/**/
		this.init=false;
		
	}
	public double getDeger() {
		return deger;
	}
	public void setDeger(double deger) {
		this.deger = deger;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isInit() {
		return init;
	}
	public void setInit(boolean init) {
		this.init = init;
	}
	

}
