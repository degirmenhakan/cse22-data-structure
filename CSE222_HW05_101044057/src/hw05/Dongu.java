package hw05;

public class Dongu {
	
	private int baslangic_satiri;
	private String counterDegiskeni;
	private int bitir_satiri;
	private int degiskenSayisi=0;
	
	public int getDegiskenSayisi() {
		return degiskenSayisi;
	}
	public void setDegiskenSayisi(int degiskenSayisi) {
		this.degiskenSayisi = degiskenSayisi;
	}
	public Dongu(int baslangic_satiri, String counterDegiskeni, int bitir_satiri) {
		super();
		this.baslangic_satiri = baslangic_satiri;
		this.counterDegiskeni = counterDegiskeni;
		this.bitir_satiri = bitir_satiri;
	}
	public int getBaslangic_satiri() {
		return baslangic_satiri;
	}
	public void setBaslangic_satiri(int baslangic_satiri) {
		this.baslangic_satiri = baslangic_satiri;
	}
	public String getCounterDegiskeni() {
		return counterDegiskeni;
	}
	public void setCounterDegiskeni(String counterDegiskeni) {
		this.counterDegiskeni = counterDegiskeni;
	}
	public int getBitir_satiri() {
		return bitir_satiri;
	}
	public void setBitir_satiri(int bitir_satiri) {
		this.bitir_satiri = bitir_satiri;
	}
	
	
	

}
