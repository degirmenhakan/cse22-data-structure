package hw05;

import hw05.InfixToPostfix.SyntaxErrorException;

import java.util.LinkedList;
import java.util.Scanner;
import java.util.Stack;

public class Evaluation {
	

	public static class NotInitialize extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public NotInitialize(String str) {
			super(str);
		}

	}

	public static class UnDefinedVariable extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public UnDefinedVariable(String str) {
			super(str);
			
		}
		
	}

	private Stack<Dongu> donguStack = new Stack<>();/* gelecek donguler icin */
	private Stack<Variable> degiskenStack = new Stack<>();/*
																	 * loopdan
																	 * gelecek
																	 * local
																	 * degiskenler
																	 * icin
																	 */
	private LinkedList<String> satir;

	private String[] satir_parcalari = null;
	private int loopdakiDegiskenSayisi=0;
	private Dongu d1 = new Dongu(0," ",0);
	private Scanner scan;
	
	
	public Evaluation(LinkedList<String> _satirlar) {
	
		satir = _satirlar;


	}

	public void basla() throws SyntaxErrorException,
			hw05.PostfixEvaluator.SyntaxErrorException {
		try {
			tumSatirlariIsle();
		} catch (NullPointerException e) {

		}
	}

	public void tumSatirlariIsle() throws SyntaxErrorException,
			hw05.PostfixEvaluator.SyntaxErrorException {
		
		for(int satir_no=0;satir_no<satir.size();){
			satir_parcalari =satir.get(satir_no).split(" ");
			
		
			if (satir_parcalari[0].equals("var")) {
				if (satir_parcalari.length == 2) {
					
						Variable var1 = new Variable(satir_parcalari[1]);
						int loopSonu = 0;
						int loopBasi = 0 ;
						
						if(!donguStack.empty()){
							
							loopSonu=donguStack.peek().getBitir_satiri();
							loopBasi = donguStack.peek().getBaslangic_satiri();
							
							
						}
						
						if(satir_no>loopBasi && satir_no<loopSonu)
						{
							int varIndex = isVariable(satir_parcalari[1]);
							
							if(varIndex==-1){
								degiskenStack.push(var1);
								if(!donguStack.empty()){
									int numberOfVar =donguStack.peek().getDegiskenSayisi();
									donguStack.peek().setDegiskenSayisi(numberOfVar+1);/*Lokal degiskenlerin sayisini tutuyor*/
								}
							}
						}
						else{
							int varIndex = isVariable(satir_parcalari[1]);
							
							if(varIndex == -1)
								degiskenStack.push(var1);
							else{
								System.err.println(satir_no+". satirda "+satir_parcalari[1]+" daha onceden tanimlanmistir.");
								System.exit(1);
							}
						}
					}else {
						
						new SyntaxErrorException(satir_no
								+ ". satirda birden fazla tanimlama yapilamaz.");
						System.exit(1);	
					}
	
				
	
			} else if (satir_parcalari[0].equals("print")) {
	
				if (satir_parcalari.length == 2) {
					int varSira = 0;
					varSira = isVariable(satir_parcalari[1]);
					if (varSira != -1) {
						if (degiskenStack.get(varSira).isInit()) {
							System.out.println(degiskenStack.get(varSira).getDeger());
	
						} else {
							new NotInitialize(satir_no
									+ ". satirda " + satir_parcalari[1]);
							System.exit(1);
	
						}
	
					} else {
	
						new UnDefinedVariable(satir_no
								+ ". satirda " + satir_parcalari[1]);
						System.exit(1);
					}
	
				} else if (satir_parcalari.length > 2) {
					System.err
							.println("Hata: "
									+ satir_no
									+ ".satirda. Ayni satirda birden fazla yazdirma yapilamaz");
					System.exit(1);
				}
	
				else {
	
					new SyntaxErrorException(satir_no
							+ ". satirda yazdirilacak degiskeni girmediniz.");
					System.exit(1);
				}
	
			} else if (satir_parcalari[0].equals("input")) {
				if (satir_parcalari.length == 2) {
					int varNo = isVariable(satir_parcalari[1]);
					
					if (varNo >= 0) {
						scan = new Scanner(System.in);
						double yeniSayi = scan.nextDouble();
						degiskenStack.get(varNo).setDeger(yeniSayi);
						degiskenStack.get(varNo).setInit(true);
	
					} else {
	
						new UnDefinedVariable(satir_no
								+ ". satirda " + satir_parcalari[1]);
						System.exit(1);
					}
	
				} else if (satir_parcalari.length > 2) {
					System.err
							.println("Hata: "
									+ satir_no
									+ ".satirda. Ayni satirda birden fazla veri girme yapilamaz");
					System.exit(1);
				} else if (satir_parcalari.length == 1) {
	
					System.err.println(satir_no
							+ ". satirda girilecek degiskeni girmediniz.");
					System.exit(1);
				}
				
			} else if (isVariable(satir_parcalari[0]) >= 0) {/*
															 * Hesaplama ifadesi
															 * geldi ise
															 */
				if (satir_parcalari[1].equals("=")) {
					String msg = "";
					InfixToPostfix infix = new InfixToPostfix();
					PostfixEvaluator posfix = new PostfixEvaluator();
					String[] expStr = new String[satir_parcalari.length - 2];
					System.arraycopy(satir_parcalari, 2, expStr, 0,
							satir_parcalari.length - 2);
					
					for (int i = 0; i < expStr.length; ++i) {
						int indexOfVar = 0;
						indexOfVar = isVariable(expStr[i]);
						
						if (indexOfVar != -1) {/*
												 * degisken gordugu yere degerini
												 * koyuyor
												 */
							if (degiskenStack.get(indexOfVar).isInit())
								expStr[i] = String.valueOf(degiskenStack.get(
										indexOfVar).getDeger());
							else
								new NotInitialize(satir_no + ".satirda "
										+ degiskenStack.get(indexOfVar).getName()
										+ " ilklendirilmemiş.");
						}else if(! expStr.equals("sin") && ! expStr.equals("cos") && ! expStr.equals("exp") &&
								! expStr.equals("tan") && ! expStr.equals("log") &&! expStr.equals("sqrt") && ! expStr.equals("asb")
								&& Character.isAlphabetic(expStr[0].charAt(0))){
								
								new UnDefinedVariable(satir_no+" satirda "+expStr[i]+" daha önce tanimlanmamis.");
								System.exit(1);
						}
	
					}
	
					/*
					 * Düzenli string arryimin her elemanini bi string yapip
					 * infixtoposfixe gonderecegim
					 */
					for (int i = 0; i < expStr.length; ++i)
						msg += " " + expStr[i];
					
					String strPostfix = infix.convert(msg);
					
					double sonuc = posfix.eval(strPostfix);
					
					int indexOfVar = isVariable(satir_parcalari[0]);
					
					degiskenStack.get(indexOfVar).setDeger(sonuc);
					degiskenStack.get(indexOfVar).setInit(true);
					
				}
			} else if (satir_parcalari[0].equals("loop")) {
				
				if (satir_parcalari.length == 2) {
					
					int varIndex = isVariable(satir_parcalari[1]);
					
					if (varIndex == -1) { /* Dongu sayacı degisken degilse */
						new UnDefinedVariable(satir_no + ". satirda "
								+ satir_parcalari[1] + " degiskeni tanimlanmamis.");
	
					} else {
						
						if (degiskenStack.get(varIndex).isInit()) {
							if(degiskenStack.get(varIndex).getDeger()<=0){
							
								break;
							}

							
							/* Bir sonraki satirda begin gelmesi gerekiyor */
							if (satir.get(satir_no + 1).equals("begin")) {
								int index=0;
								
								int end=satir.size();
		
								for(index=satir_no+2;index<satir.size();++index){
									if(satir.get(index).equals("end")){
										end=index;
										break;
									}
									
								}
		
								
								
								d1 = new Dongu(satir_no + 2, degiskenStack.get(
										varIndex).getName(), end);
								
								
								/*
								 * loop satackindeki elemana bak bunun degiskeni
								 * simdiki degiskense sonsuz donguye girmemsi icin
								 * ekleme yapmasın
								 */
								if (!donguStack.empty()) {
									d1.setDegiskenSayisi(loopdakiDegiskenSayisi);
									if (degiskenStack
											.get(varIndex)
											.getName()
											.equals(donguStack.peek()
													.getCounterDegiskeni()))
										;
									else {/* degilse bu donguyu stacke koy */
										donguStack.push(d1);
									}
								}
								else if (donguStack.empty()) {/*
														 * eger dongu icinde
														 * degilsem yani dongu
														 * stagim bos ise
														 */
									donguStack.push(d1);/*
														 * Yeni baslayan donguyu
														 * stacke koyuyorum
														 */
								}
								/*
								 * loop var ve begin den sonraki satirlara gecmek
								 * icin
								 */
								 
								satir_no += 2;
								
								continue; /*begine yakalanmamasi lazım*/
							}
	
						}/*TODO*/
						 else {/* dongu sayacı ilkllendirilmemisse */
								
								new NotInitialize(satir_no + ". satirda "
										+ degiskenStack.get(varIndex).getName()
										+ " ilklendirilmemis.");
	
							}
						
	
					}
	
				}else{
					
					new SyntaxErrorException(satir_no+". satirda loopda 1 adet arguman bulunmali.");
					System.exit(1);
				}
			} else if (satir_parcalari[0].equals("end")) {
				
				String counterName = null;
				if(!donguStack.empty())
					counterName = donguStack.peek().getCounterDegiskeni();
				
				
				int varIndex = isVariable(counterName);
				if (varIndex == -1) {/*Buraya donguStack veya degisken stack yanlislikla pop edildi ise girecek*/
					System.err.println("Bir problem var...");
					System.exit(1);
				} else {
					
					if (degiskenStack.get(varIndex).getDeger() < 0) {/*
																	 * eger counter
																	 * 0 a ulasti
																	 * ise dongu
																	 * sonlansın
																	 			*/
						
						if (!donguStack.empty()) {/*
												 * dongu bos degilse loopu kaldır
												 * stackden
												 */
							
							for(int i=0;i<donguStack.peek().getDegiskenSayisi();++i){
								
								degiskenStack.pop();
								
							}
							
							loopdakiDegiskenSayisi=0;
							donguStack.pop();
							
						} else {/* loop olmadan end yazildi ise */
							new SyntaxErrorException(
									satir_no
											+ ". satir da dongu olmadan baslamadan end gelmis.");
							System.exit(1);
						}
	
					} else {
					
						satir_no = donguStack.peek().getBaslangic_satiri();/*
																			 * eger
																			 * counter
																			 * 0 a
																			 * ulasmadi
																			 * ise
																			 * islem
																			 * devam
																			 * edecek
																			 */
					
						continue;
					}
	
				}
	
			}else if(satir_parcalari[0].equals("begin")){
				/*begini tanımasi icin yaptım*/
				
			}else {
				System.err.println(satir_no + ". satir systax error. "+satir_parcalari[0]);
				System.exit(1);
	
			}
			satir_no++;
		}
	}

	public int isVariable(String name) {
		

		
		for (int i = 0; i < degiskenStack.size(); i++) {
			
			if (name.equals(degiskenStack.get(i).getName())){
				
				return i;
			}
		}

		return -1;
	}


	

}
